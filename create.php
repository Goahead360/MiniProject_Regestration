<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Mini Project Registratio </title>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="custom.css">
</head>
<body>

<div class="container-fluid">
	<nav class="navbar navbar-default" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">Mini Project #2</a>
		</div>
	
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.php">View</a></li>
				<li><a href="create.php">Registration</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</nav>
</div>

<div class="container-fluid header">
	<div class="row">
		<div class="col-md-offset-3 col-md-6 text-center">
			<h1>Add New User</h1>
		</div>
	</div>	
</div>

<div class="jumbotron">
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
				<form role="form" action="store.php" method="POST">		
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-fw fa-user"></i>
							</div>
						<input class="form-control input-lg" placeholder="First Name"  type="text" autofocus="autofocus">
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-fw fa-user"></i>
							</div>
						<input class="form-control input-lg" placeholder="Last Name"  type="text"></div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-fw fa-group"></i>
							</div>
							<input class="form-control input-lg " placeholder="Organization Name"  type="text">
						</div>
					</div>

					<div class="form-group" >
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-fw fa-envelope-o"></i>
							</div>
							<input class="form-control input-lg " placeholder="E-mail"  type="text">
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-fw fa-key"></i>
							</div>
							<input class="form-control input-lg" placeholder="Choose Password"  type="password" data-toggle="tooltip" data-placement="right" title="Choose Your Password">
						</div>
					</div>

					<div class="form-group"><p class="checkbox text-center">
						<label>
						<input class="" style="vertical-align: middle" id="agree"  type="checkbox"> I Agree to the 
						<a class="text-success" href="#" target="_blank" tabindex="8">Terms of Service</a> 
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-success btn-lg btn-block" >Complete Registration
						</button>
					</div>
				</form>
			</div>
		</div>	
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
</body>
</html>